#ifndef AC_H_
#define AC_H_

class AC{
    int temperatur, fanSpeed;
    bool swing, power;

    public:
        void setTemperatur(int);
        void setSwing(bool);
        void setPower(bool, int, bool);
        void setFanSpeend(int);
        int getTemperatur(){ return temperatur; };
        int getFanSpeen(){ return fanSpeed; };
        int getSwing(){ return swing; };
};

void AC::setTemperatur(int temperatur){
    this->temperatur = temperatur;
}

void AC::setSwing(bool swing){
    this->swing = swing;
}

void AC::setFanSpeend(int fanSpeed){
    this->fanSpeed = fanSpeed;
}

void AC::setPower(bool power, int fanSpeed, bool swing){
    this->power = power;
    this->fanSpeed = fanSpeed;
    this->swing = swing;
}

#endif
