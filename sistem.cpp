#include<iostream>
#include<math.h>
#include<string>
#include<stdlib.h>
#include "AC.h"
#include "Sensor.h"

using namespace std;

int checkJumlahOrang(int jumlahOrang){
    int hasil;
    if(jumlahOrang>=0 && jumlahOrang<=14)
        hasil=0;
    else if(jumlahOrang>=15 && jumlahOrang<=25)
        hasil=1;
    else
        hasil=2;

return hasil;
}
int checkKelembaban(float kelembaban){
    int hasil;
    if(kelembaban>=0 && kelembaban<=25)
        hasil=0;
    else if(kelembaban>=26 && kelembaban<=69)
        hasil=1;
    else
        hasil=2;

return hasil;
}

void setAturan(AC *objekAC, Sensor *objekSensor, string tableAturan[3][3], int rangeSuhu[3][2]){
    cout << "masuk setAturan" << endl;
    string aturan = tableAturan[objekSensor->getTingkatJumlahOrang()][objekSensor->getTingkatKelembaban()];

    int koma[2], ubahTemperatur, indeksRangeSuhu, ubahFanSpeed;

    koma[0] = aturan.find_first_of(",");
    koma[1] = aturan.find_last_of(",");

    //ganti temperatur
    ubahTemperatur = atoi(aturan.substr(0,koma[0]).c_str());
    cout << "ubah temperatur : " << ubahTemperatur << endl;
    indeksRangeSuhu = objekSensor->getTingkatJumlahOrang();
    if(objekAC->getTemperatur()+ubahTemperatur>=rangeSuhu[indeksRangeSuhu][0] && objekAC->getTemperatur()+ubahTemperatur<=rangeSuhu[indeksRangeSuhu][1])
        objekAC->setTemperatur(objekAC->getTemperatur()+ubahTemperatur);

    cout << "temperatur in setAturan : " << objekAC->getTemperatur() << endl;
    //set swing
    if(aturan.substr(koma[1]+1,1).compare("0")==0)
        objekAC->setSwing(false);
    else
        objekAC->setSwing(true);

    //set fan speen
    ubahFanSpeed = atoi(aturan.substr(koma[0]+1,koma[1]-koma[0]-1).c_str());
    objekAC->setFanSpeend(ubahFanSpeed);

    return;

    //cout << aturan << endl;
    //cout << aturan.substr(0,koma[0]) << endl;
    //cout << aturan.substr(koma[0]+1,koma[1]-koma[0]-1) << endl;
    //cout << aturan.substr(koma[1]+1,1) << endl;

    //cout << atoi(aturan.substr(0,1).c_str())+atoi(aturan.substr(2,1).c_str()) << endl;
}

int main(){

    string tableAturan[3][3] = { {"0,1,0","0,1,0","1,1,0"},
                        {"0,2,1","-1,2,1","-1,1,0"},
                        {"-2,3,1","-1,2,1","0,1,1"}};

    //rangeSuhu digunakan untuk membatasi perubahan suhu berdasarkan jumlah orang, rendah sedang tinggi
    int rangeSuhu[3][2] = { {24,27},
                            {19,23},
                            {15,18}};
    int indeksRangeSuhu, nilaiTengahRangeSuhu, tingkatJumlahOrangSebelum;

    Sensor objekSensor(-1);
    AC objekAC;

    int jumlahOrang, tingkatKelembaban, tingkatJumlahOrang;
    float kelembaban;
    while(true){
        cout << "Program akan berjalan terus, tekan CTRL+C untuk exit" << endl;
        cout << "Jumlah orang : " << endl;
        cin >> jumlahOrang;
        cout << "Kelembaban : " << endl;
        cin >> kelembaban;

        if(objekSensor.getTingkatJumlahOrang()==-1){
            objekAC.setPower(true, 0, false);
        }

        if(jumlahOrang==0){
            objekAC.setPower(false, 0, false);
            objekSensor.setTingkatJumlahOrangKelembaban(-1, -1);
            break;
        }


        objekSensor.setJumlahOrangKelembaban(jumlahOrang, kelembaban);
        tingkatJumlahOrangSebelum = objekSensor.getTingkatJumlahOrang();
        objekSensor.setTingkatJumlahOrangKelembaban(checkJumlahOrang(objekSensor.getJumlahOrang()), checkKelembaban(objekSensor.getKelembaban()));

        //objek baru terbentuk
        if(tingkatJumlahOrangSebelum == -1 || tingkatJumlahOrangSebelum!=objekSensor.getTingkatJumlahOrang()){
            indeksRangeSuhu = objekSensor.getTingkatJumlahOrang();
            cout << "indeks range suhu : " << indeksRangeSuhu << endl;
            nilaiTengahRangeSuhu = floor((rangeSuhu[indeksRangeSuhu][1]-rangeSuhu[indeksRangeSuhu][0])/2);
            cout << "nilai tengah range suhu : " << nilaiTengahRangeSuhu << endl;
            objekAC.setTemperatur(nilaiTengahRangeSuhu+rangeSuhu[indeksRangeSuhu][0]);
            cout << "temperatur in if : " << objekAC.getTemperatur() << endl;
        }
        setAturan(&objekAC, &objekSensor, tableAturan, rangeSuhu);

        cout << "Temperatur sekarang : " << objekAC.getTemperatur() << endl;
        cout << "Fan Speed sekarang : " << objekAC.getFanSpeen() << endl;
        cout << "Swing sekarang : " << objekAC.getSwing() << endl;
    }

return 0;
}
