#ifndef Sensor_H_
#define Sensor_H_

class Sensor{
    int jumlahOrang, tingkatJumlahOrang, tingkatKelembaban;
    float kelembaban;

    public:
        Sensor(int);
        void setTingkatJumlahOrangKelembaban(int,int);
        void setJumlahOrangKelembaban(int, float);
        int getJumlahOrang(){ return jumlahOrang; };
        float getKelembaban(){ return kelembaban; };
        int getTingkatJumlahOrang(){ return tingkatJumlahOrang;};
        int getTingkatKelembaban(){ return tingkatKelembaban;};
};

void Sensor::setJumlahOrangKelembaban(int jumlahOrang, float kelembaban){
    this->jumlahOrang = jumlahOrang;
    this->kelembaban = kelembaban;
}

void Sensor::setTingkatJumlahOrangKelembaban(int tingkatJumlahOrang, int tingkatKelembaban){
    this->tingkatJumlahOrang = tingkatJumlahOrang;
    this->tingkatKelembaban = tingkatKelembaban;
}

Sensor::Sensor(int tingkatJumlahOrang){
    this->tingkatJumlahOrang = tingkatJumlahOrang;
}

#endif
